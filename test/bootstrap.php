<?php

ini_set('display_errors', 1);

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../src/app.php';
require_once __DIR__.'/../config/dev.php';
require_once __DIR__.'/../src/controllers.php';
