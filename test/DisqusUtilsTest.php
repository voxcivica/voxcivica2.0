<?php

class DisqusUtilsTest extends \PHPUnit_Framework_TestCase {

  public function test_disqus_lang_changes_es_to_es_ES() {
    $this->assertEquals('es_ES', Disqus\Utils::disqus_lang('es'));
  }
}