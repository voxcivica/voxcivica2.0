<?php
namespace Repository;

use Domain\Contact;

class TeamRepo {
  private static $database;

  public function members() {
    if (null == self::$database)
      self::readData();
    return array_filter(self::$database, function(Contact $contact) {
      return $contact->type == Contact::MEMBER;
    });
  }

  public function advisors() {
    if (null == self::$database)
      self::readData();
    return array_filter(self::$database, function(Contact $contact) {
      return $contact->type == Contact::ADVISOR;
    });
  }

  private static function readData() {
    $cacheFilePath  = __DIR__ . '/../Domain/data/team.serialized';
    self::$database = unserialize(file_get_contents($cacheFilePath));
  }
}