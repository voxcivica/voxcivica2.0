<?php
namespace Repository;

use Cache\PostCache;

class CachedPostRepo extends PostRepo {
  private $directPostRepo;
  private $cache;

  function __construct(DirectPostRepo $postRepo, PostCache $cache) {
    $this->directPostRepo = $postRepo;
    $this->cache          = $cache;
  }

  function updatePostCache($slug) {
    $this->cache->put($slug, $this->directPostRepo->withSlug($slug));
    $this->updateHeadCache();
  }

  function updateHeadCache() {
    $this->cache->put('head', $this->directPostRepo->headPage());
  }

  function headPage() {
    if (!$this->cache->contains('head'))
      $this->updateHeadCache();
    return $this->cache->get('head');
  }

  function withSlug($slug) {
    if (!$this->cache->contains($slug))
      $this->updatePostCache($slug);
    return $this->cache->get($slug);
  }
}