<?php
namespace Repository;

class DirectPostRepo extends PostRepo {
  private $blogUrl;

  function __construct($blogUrl) {
    $this->blogUrl = $blogUrl;
  }

  public function headPage() {
    $json = file_get_contents("{$this->blogUrl}/?json=1&post_type=post");
    return $this->buildPostCollection($json);
  }

  public function withSlug($slug) {
    $json = file_get_contents("{$this->blogUrl}/?json=get_post&slug={$slug}");
    return $this->buildPost($json);
  }
}