<?php
namespace Repository;

use Domain\Post;

abstract class PostRepo {
  abstract function headPage();

  abstract function withSlug($slug);

  protected function buildPost($json) {
    if (null == $json)
      return null;
    $data = json_decode($json)->post;
    return Post::factory($data);
  }

  protected function buildPostCollection($json) {
    if (null == $json)
      return array();
    $posts = array();
    foreach (json_decode($json)->posts as $data)
      $posts[] = Post::factory($data);
    return $posts;
  }
}



