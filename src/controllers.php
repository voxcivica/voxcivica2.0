<?php
/** @var $app Silex\Application */
use Controller\MicrositeController;
use Controller\BlogController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

$app->before(function (Request $request) use ($app) {
  $lang = $request->get('lang');
  if (in_array($lang, array('es', 'en', 'eu')))
    $app['translator']->setLocale($request->get('lang'));
});

$app->error(function (\Exception $e, $code) use ($app) {
  if ($app['debug'])
    return null;
  $page = 404 == $code ? '404.html' : '500.html';
  return new Response($app['twig']->render($page, array('code' => $code)), $code);
});

$app->post('/send-contact-email', function(Request $request) use ($app) {
  $subject = 'Web VoxCivica - Formulario de contacto';
  $nombre  = $request->get('nombre');
  $email   = $request->get('email');
  $mensaje = $request->get('mensaje');
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('hola@voxcivica.org' => 'Hola VoxCivica'))
      ->setTo(array('hola@voxcivica.org'))
      ->setBody("{$nombre}\n{$email}\n\n{$mensaje}");
  if ($app['mailer']->send($message))
    return json_encode(array('result'=> 'ok'));
  return json_encode(array('result'=> 'ko', 'error'=> 'send error'));
})
    ->bind('send-contact-email');

$app->get('/{lang}', function ($lang) use ($app) {
  return $app['twig']->render('home/index.twig', array(
    'lang'         => $lang,
    'current_path' => '',
    'members'      => $app['team']->members(),
    'advisors'     => $app['team']->advisors()
  ));
})
    ->assert('lang', 'es|eu|en')
    ->bind('home')
    ->value('lang', 'es');

$app->get('/{lang}/credits', function ($lang) use ($app) {
  return $app['twig']->render('credits.twig', array(
    'lang'         => $lang,
    'current_path' => '/credits'
  ));
})
    ->assert('lang', 'es|eu|en')
    ->bind('credits');

$app->get('/{lang}/legal', function ($lang) use ($app) {
  return $app['twig']->render('legal.twig', array(
    'lang'         => $lang,
    'current_path' => '/legal'
  ));
})
    ->assert('lang', 'es|eu|en')
    ->bind('legal');

$app->get('/{lang}/privacy', function ($lang) use ($app) {
  return $app['twig']->render('privacy.twig', array(
    'lang'         => $lang,
    'current_path' => '/privacy'
  ));
})
    ->assert('lang', 'es|eu|en')
    ->bind('privacy');

$app->mount('/', new BlogController());
$app->mount('/', new MicrositeController());
