<?php
namespace Domain;

class Post {
  public $author;
  public $date;
  public $slug;
  public $title;
  public $content;
  public $excerpt;
  public $cachedDate;

  function __construct() {
    $this->cachedDate = time();
  }

  public static function factory($data) {
    $post          = new Post();
    $post->author  = $data->author->name;
    $post->date    = $data->date;
    $post->slug    = $data->slug;
    $post->title   = $data->title;
    $post->content = $data->content;
    $post->excerpt = $data->excerpt;
    return $post;
  }
}