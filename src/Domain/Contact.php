<?php

namespace Domain;

class Contact {
  const MEMBER  = 'member';
  const ADVISOR = 'advisor';

  public $type = '';
  public $name = '';
  public $nameLong = '';
  public $slug = '';
  public $role = '';
  public $links = array();

  static function factory($type, $name, $nameLong, $slug, $role, $twitter, $blog, $linkedin) {
    $contact           = new Contact();
    $contact->type     = $type;
    $contact->name     = $name;
    $contact->nameLong = $nameLong;
    $contact->slug     = $slug;
    $contact->role     = $role;
    if ("" !== $twitter)
      $contact->links[] = array('type' => 'twitter', 'url' => "http://twitter.com/{$twitter}");
    if ("" !== $blog)
      $contact->links[] = array('type' => 'blog', 'url' => $blog);
    if ("" !== $linkedin)
      $contact->links[] = array('type' => 'linkedin', 'url' => $linkedin);
    return $contact;
  }
}
