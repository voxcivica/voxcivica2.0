<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;

class MicrositeController implements ControllerProviderInterface {
  public function connect(Application $app) {
    $controllers = $app['controllers_factory'];

    $controllers->get('/{lang}/bitforchange', function($lang) use($app) {
      return $app['twig']->render('bitforchange/index.twig', array(
        'lang' => $lang
      ));
    })
        ->assert('lang', 'es|eu|en')
        ->bind('bitforchange');

    return $controllers;
  }
}