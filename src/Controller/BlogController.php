<?php

namespace Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Silex\ControllerProviderInterface;

class BlogController implements ControllerProviderInterface {
  public function connect(Application $app) {
    $controllers = $app['controllers_factory'];

    $controllers->post('/blog/updateCache', function(Request $request) use($app) {
      $slug = $request->get('slug');
      $app['posts']->updatePostCache($slug);
      return '';
    })
        ->bind('blog_update_cache');

    $controllers->get('/{lang}/blog', function ($lang) use ($app) {
      return $app['twig']->render('blog/index.twig', array(
        'lang'         => $lang,
        'current_path' => '/blog',
        'posts'        => $app['posts']->headPage()
      ));
    })
        ->assert('lang', 'es|eu|en')
        ->bind('blog');

    $controllers->get('/{lang}/blog/{slug}', function ($lang, $slug) use ($app) {
      return $app['twig']->render('blog/post.twig', array(
        'lang'         => $lang,
        'current_path' => "/blog/{$slug}",
        'post'         => $app['posts']->withSlug($slug)
      ));
    })
        ->assert('lang', 'es|eu|en')
        ->bind('blog_post');

    return $controllers;
  }
}