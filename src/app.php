<?php

use Repository\DirectPostRepo;
use Repository\CachedPostRepo;
use Repository\TeamRepo;
use Cache\PostCache;
use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\HttpCacheServiceProvider;
use Symfony\Component\Translation\Loader\YamlFileLoader;

date_default_timezone_set('Europe/Madrid');

$app = new Application();
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  'twig.path'    => __DIR__ . '/../templates',
  'twig.options' => array('cache' => __DIR__ . '/../cache')
  //'twig.options' => array()
));
$app->register(new HttpCacheServiceProvider(), array(
  'http_cache.cache_dir' => __DIR__ . '/../cache/http',
  'http_cache.esi'       => null,
));
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app['twig'] = $app->share($app->extend('twig', function(Twig_Environment $twig) {
  $twig->addFilter('disqus_lang', new Twig_Filter_Function('Disqus\\Utils::disqus_lang'));
  return $twig;
}));

$app['team'] = $app->share(function () {
  return new TeamRepo();
});

$app['posts'] = $app->share(function () {
  return new CachedPostRepo(
    new DirectPostRepo('http://cms.voxcivica.org'),
    new PostCache(__DIR__ . '/../cache/Post.cache')
  );
});

$app['translator'] = $app->share($app->extend('translator', function (\Symfony\Component\Translation\Translator $translator, $app) {
  $translator->addLoader('yaml', new YamlFileLoader());
  $translator->addResource('yaml', __DIR__ . '/../i18n/en.yml', 'en');
  $translator->addResource('yaml', __DIR__ . '/../i18n/es.yml', 'es');
  $translator->addResource('yaml', __DIR__ . '/../i18n/eu.yml', 'eu');
  return $translator;
}));

$app['swiftmailer.options'] = array(
  'host' => 'smtp.gmail.com',
  'port' => '465',
  'username' => 'hola@voxcivica.org',
  'password' => 'V0xC1v1c4',
  'encryption' => 'ssl',
  'auth_mode' => null
);

return $app;
