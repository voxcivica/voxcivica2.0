<?php

namespace Disqus;

class Utils {
  static function disqus_lang($input_lang) {
    return ('es' != $input_lang) ? $input_lang : 'es_ES';
  }
}