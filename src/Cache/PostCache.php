<?php

namespace Cache;

class PostCache {
  private $file;
  private $storage;

  function __construct($file) {
    if (!file_exists($file) && !touch($file))
      throw new \Exception("Can' use {$file} as cache location for Posts");
    $cachedData    = file_get_contents($file);
    $this->storage = ('' !== $cachedData) ? unserialize($cachedData) : array();
    $this->file    = $file;
  }

  function put($key, $value) {
    $this->storage[$key] = $value;
    $this->refreshCacheFile();
  }

  function get($key) {
    return $this->storage[$key];
  }

  public function contains($key) {
    return array_key_exists($key, $this->storage);
  }

  private function refreshCacheFile() {
    file_put_contents($this->file, serialize($this->storage));
  }
}