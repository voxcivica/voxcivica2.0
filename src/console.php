<?php
/** @var $app Silex\Application */
use Symfony\Component\Console\Application;
use Domain\Contact;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

$console = new Application('VoxCivica Command-line', 'n/a');
$console
    ->register('reload-members')
    ->setDefinition(array())
// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
    ->setDescription('Reload team members from CSV file located at src/Domain/data.csv')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
  $lines  = explode("\n", file_get_contents(__DIR__ . "/Domain/data/team.csv"));
  $header = array_shift($lines);

  $contacts = array();
  foreach ($lines as $line) {
    list($type, $name, $nameLong, $slug, $role, $twitter, $blog, $linkedin) = extractFields($line);
    $contacts[] = Contact::factory($type, $name, $nameLong, $slug, $role, $twitter, $blog, $linkedin);
  }

  file_put_contents(__DIR__ . "/Domain/data/team.serialized", serialize($contacts));
});

function strip_quotes($text) {
  $text = trim($text);
  if ('' === $text || '"' !== $text[0])
    return $text;
  return substr($text, 1, strlen($text) - 2);
}

function extractFields($line) {
  $fields   = explode(';', $line);
  $type     = strip_quotes($fields[0]);
  $name     = strip_quotes($fields[1]);
  $nameLong = strip_quotes($fields[2]);
  $slug     = strip_quotes($fields[3]);
  $role     = strip_quotes($fields[4]);
  $twitter  = strip_quotes($fields[5]);
  $blog     = strip_quotes($fields[6]);
  $linkedin = strip_quotes($fields[7]);
  return array($type, $name, $nameLong, $slug, $role, $twitter, $blog, $linkedin);
}

return $console;