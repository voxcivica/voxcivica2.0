window.voxcivica = window.voxcivica || {};

(function (voxcivica, undefined) {
  var scrollOptions = {
    hash:true,
    duration:400,
    offset:-60
  };

  voxcivica.Scrolls = function () {
    return {
      init:function () {
        $.localScroll(scrollOptions);
      }
    };
  };
}(window.voxcivica));