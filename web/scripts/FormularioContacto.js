window.voxcivica = window.voxcivica || {};

(function (voxcivica, undefined) {

  voxcivica.FormularioContacto = function (selector) {
    var contacto = $(selector);
    var form = $(contacto.find('form'));

    var activateSubmit = function () {
      form.find('button').on('click', function (e) {
        e.preventDefault();
        if (isValid())
          send();
        return false;
      });
    };

    var activateAnchors = function() {
      $('a[data-body]').on('click', function(e) {
        $(selector + '_mensaje').val($(this).data().body);
      });
    };

    var send = function () {
      var url = form.attr('action');
      $.post(url, form.serialize(), function (response) {
        var mensaje = 'ok' == response.result ? contacto.find('.exito') : contacto.find('.fracaso');
        form.hide();
        mensaje.stop(true, true).fadeIn(250).delay(5000).fadeOut(250).queue(function () {
          reset();
          form.show();
        });
      }, 'json');
    };

    var reset = function () {
      contacto.find('.resultado').hide().queue(function () {
        form.find('.control-group').removeClass('error').removeClass('success');
        form.find('.help-inline').hide();
        form.find('input').val('');
        form.find('textarea').val('');
      });
    };

    var renderErrors = function (validation) {
      for (var fieldName in validation)
        if (validation.hasOwnProperty(fieldName)) {
          var field = $(selector + '_' + fieldName);
          if (validation[fieldName]) {
            field.parents('.control-group').removeClass('error');
            field.siblings('.help-inline').hide();
          } else {
            field.parents('.control-group').addClass('error');
            field.siblings('.help-inline').show();
          }
        }
    };

    var isValid = function () {
      var validation = validate();
      renderErrors(validation);
      return (validation.nombre && validation.email && validation.mensaje);
    };

    var validate = function () {
      var validation = {
        nombre:true,
        email:true,
        mensaje:true
      };
      for (var field in validation)
        if (validation.hasOwnProperty(field))
          if ('' == $(selector + '_' + field).val())
            validation[field] = false;
      validation.tos = 'checked' == $(selector + '_tos').attr('checked');
      return validation;
    };

    return {
      init:function () {
        activateSubmit();
        activateAnchors();
        return this;
      }
    };
  };
}(window.voxcivica));