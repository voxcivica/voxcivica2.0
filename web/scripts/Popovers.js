window.voxcivica = window.voxcivica || {};

(function (voxcivica, undefined) {
  voxcivica.Popovers = function (selector, template) {
    var visible, clicked;

    function haveToHideEm() {
      return visible && clicked;
    }

    function reset() {
      visible = clicked = false;
    }

    function hideEm() {
      $(selector).popover('hide');
    }

    function popoverIsVisible() {
      visible = true;
    }

    function extractContent(id) {
      return $('#popover-' + id).html();
    }

    function setupPopovers() {
      $(selector).each(function (idx, it) {
        var id = $(it).attr('id');
        $(it)
            .popover({
              animation:true,
              html:true,
              content:extractContent.bind(undefined, id),
              template:template
            })
            .click(function (e) {
              hideEm();
              $(this).popover('show');
              popoverIsVisible();
              e.preventDefault()
            });
      });
    }

    function clickedOnAPopoverable(target) {
      return $(target).hasClass('miembro') || $(target).parents('.miembro').length > 0;
    }

    function listenToOtherClicks() {
      $(document).click(function (e) {
        if (!clickedOnAPopoverable(e.target) && haveToHideEm()) {
          hideEm();
          reset();
        } else
          clicked = true
      });
    }

    return {
      init:function () {
        reset();
        setupPopovers();
        listenToOtherClicks();
      }
    }
  };
}(window.voxcivica));